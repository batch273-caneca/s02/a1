/*Quiz

1 What is the term given to unorganized code that's very hard to work with?
		spaghetti code
2 How are object literals written in JS?
		thru the use of {} with key-value pairs
3 What do you call the concept of organizing information and functionality to belong to an object?
		JS OOP
4 If studentOne has a method named enroll(), how would you invoke it?
		studentOne.enroll();
5 True or False: Objects can have objects as properties?
		True
6 What is the syntax in creating key-value pairs?
		{ key1: value1, key2: value2, ... }
7 True or False: A method can have no parameters and still work?
		True
8 True or False: Arrays can have objects as elements
		True
9 True or False: Arrays are objects.
		True
10 True or False: Objects can have arrays as properties.
		True
*/

//
let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

//
computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade);
        return sum/this.grades.length;
    },


//
willPass(){
        return this.computeAve() >= 85;
    },

//
willPassWithHonors() {
    return (this.willPass() && this.computeAve() >= 90) ? true : false
	}
};


// Function Coding

let studentTwo = {
    name: 'Joe' ,
    email: 'joe@mail.com' ,
    studentTwoGrades: [78, 82, 79, 85], 

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    }, 

    computeAve() {
        let sum = 0;
        this.studentTwoGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.computeAve();
       console.log(`${studentTwo.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.computeAve();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },
};

let studentThree = {
    name: 'Jane' ,
    email: 'jane@mail.com' ,
    studentThreeGrades: [87, 89, 91, 93], 

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

    computeAve() {
        let sum = 0;
        this.studentThreeGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.computeAve();
       console.log(`${studentThree.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.computeAve();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },

};

let studentFour = {
    name: 'Jessie' ,
    email: 'jessie@mail.com' ,
    studentFourGrades: [91, 89, 92, 93],  

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

    computeAve() {
        let sum = 0;
        this.studentFourGrades.forEach(grade => sum = sum + grade );
        return sum/4;
    },

    willPass() {
       const average = this.computeAve();
       console.log(`${studentFour.name} quarterly grade average is : ${average}`)
       if ( average >= 85) {
        return true
       } else {
        return false
       }
    },

    willPassWithHonors() {
        const average = this.computeAve();

        if (average >= 90) {
            return true;
        } else if (average >= 85) {
            return false;
        } else if (average < 85){
            return undefined;
        }
    },

};

const classof1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

    countHonorStudents() {
        const honorStudents = this.students.filter(student => student.willPassWithHonors() === true);
        return honorStudents.length;
    },

    honorsPercentage() {
        const totalStudents = this.students.length;
        const honorStudents = this.countHonorStudents();
        const percentage = (honorStudents / totalStudents) * 100;
        return percentage
    },

    retrieveHonorStudentInfo() {
        const honorStudents = [];
        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                email: student.email,
                averageGrade: student.computeAve()
            });
            }
        });
        return honorStudents;
    },

    sortHonorStudentsByGradeDesc() {
        const honorStudents = this.students.filter(student => student.willPassWithHonors() === true);
        const sortedStudents = honorStudents.sort((a, b) => b.computeAve() - a.computeAve());
        const studentInfo = sortedStudents.map(student => {
            return {
                email: student.email,
                averageGrade: student.computeAve(),
            };
        });
        return studentInfo;
      }

}
